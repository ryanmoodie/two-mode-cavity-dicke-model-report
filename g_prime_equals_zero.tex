\section{Case: \texorpdfstring{$g^{\prime}=0$, $\, g \in \mathbb{R}$}{g prime = 0, g real}}
\label{g-prime-equals-zero}

We investigate the regime with $g^{\prime}=0$ and real $g$ as it exhibits $U(1)$ symmetry. The Hamiltonian takes the form:
\begin{equation}
    \hat{H} = 
    \tilde{\omega}_{a} \hat{a}^{\dagger} \hat{a} 
    + \tilde{\omega}_{b} \hat{b}^{\dagger} \hat{b} 
    + \omega_{0} \hat{S}^{z} 
    + g \bigg [ \Big ( 
        \hat{a}^{\dagger} 
        + \hat{b} \Big ) \hat{S}^{-} 
     + H.c. \bigg ]
\end{equation}
In the steady state, the equations of motion (\cref{g-prime-equals-zero-eom-alpha,g-prime-equals-zero-eom-beta,g-prime-equals-zero-eom-spin-plus,g-prime-equals-zero-eom-spin-z}) become:

\noindent\begin{tabularx}{\linewidth}{X X}
{\begin{align}
    \label{g-prime-equals-zero-alpha-steady}
    \alpha_{0} &= - \frac{g S^{-}_{0}}{\tilde{\omega}_{a} - i \frac{\kappa_{a}}{2}} 
    \\
    \label{g-prime-equals-zero-s-plus-steady}
    \tilde{\omega}_{0} S^{+}_{0}
    &= 2 g \left ( \alpha_{0}^{*} + \beta_{0} \right ) S^{z}_{0} 
    \end{align}} &
{\begin{align}
    \label{g-prime-equals-zero-beta-steady}
    \beta_{0} &= - \frac{g S^{+}_{0}}{\tilde{\omega}_{b} - i \frac{\kappa_{b}}{2}} 
    \\
    \label{g-prime-equals-zero-s-z-steady}
    0 &= \left ( \alpha_{0}^{*} + \beta_{0} \right ) S^{-}_{0} - c.c.
    \end{align}}
\end{tabularx}

\subsection{Symmetry}

The $g^{\prime}=0$ Hamiltonian is symmetric under a simultaneous rotation in the basis of the modes and spins, defined by the generator:
\begin{equation}
    \hat{G}=\hat{a}^{\dagger}\hat{a}-\hat{b}^{\dagger}\hat{b}+\hat{S}^{z}
\end{equation}
Thus, $[\hat{G},\hat{H}]=0$ is satisfied and $\hat{U}\hat{H}\hat{U}^{\dagger}=\hat{H}$ for $\hat{U}=e^{i \theta \hat{G}}$ for arbitrary angle $\theta$. This corresponds to the $U(1)$ symmetry. This also holds for complex $g$, but we restrict to real $g$ to simplify the solution analytics.

As this is an open system, we must also take into account losses. We can check this by inserting the transformed density matrix $\hat{U}^{\dagger} \hat{\rho} \hat{U}$ in place of $\hat{\rho}$ in the Lindblad equation (\cref{lindblad-equation}). Rearranging such that transformation operators $\hat{U}$ sit around mode operators, \textit{i.e.} like $\hat{U} \hat{a} \hat{U}^{\dagger}$, and observing that mode operators always appear as annihilation and creation pairs in the loss terms (consider \cref{lindblad-superoperator}), we see that the transformed mode rotation factors cancel out. Thus, the symmetry also exists in the open system.

\subsection{Solutions}

Inserting \cref{g-prime-equals-zero-alpha-steady,g-prime-equals-zero-beta-steady} into \cref{g-prime-equals-zero-s-z-steady} shows that only a specific choice of the parameters will yield a superradiant solution:
\begin{equation}
    \tilde{\omega}_{b} = \pm \sqrt{\frac{\kappa_{b}}{\kappa_{a}} \tilde{\omega}_{a}^{2} + \frac{\kappa_{b}}{4}\left ( \kappa_{a}-\kappa_{b} \right ) }
\end{equation}
which in the case that $\kappa_{a} = \kappa_{b}$ is $\tilde{\omega}_{a} = \tilde{\omega}_{b}$. When this condition is not satisfied, no stationary solution exists.

However, for $g^{\prime}=0$ a rotating solution also exists. With rotation frequency $\mu$, it has the form:
\begin{align}
    \alpha =& \alpha_{0} e^{- i \mu t} &
    \beta  =& \beta_{0} e^{i \mu t} &
    S^{+}  =& S^{+}_{0} e^{i \mu t} &
    S^{z}  =& S^{z}_{0}
\end{align}
In insertion of this single-frequency rotating solution into the equations of motion, the time-dependent factors can be cancelled out on either side of each equation. The resultant effect on the solution is to shift the frequencies by the rotation frequency:
\begin{align}
    \tilde{\tilde{\omega}}_{a, b} &= \tilde{\omega}_{a, b} \mp \mu &
    \tilde{\tilde{\omega}}_{0} &= \tilde{\omega}_{0} - \mu 
\end{align}
\textit{i.e.} giving the rotating-frame solutions:

\noindent\begin{tabularx}{\linewidth}{X X}
{\begin{align}
    \label{g-prime-equals-zero-alpha-rotating}
    \alpha_{0} =& - \frac{g S^{-}_{0}}{\tilde{\tilde{\omega}}_{a} - i \frac{\kappa_{a}}{2}} 
    \\
    \label{g-prime-equals-zero-s-plus-rotating}
    \tilde{\tilde{\omega}}_{0} S^{+}_{0}
    =& 2 g \left ( \alpha_{0}^{*} + \beta_{0} \right ) S^{z}_{0} 
    \end{align}} &
{\begin{align}
    \label{g-prime-equals-zero-beta-rotating}
    \beta_{0} =& - \frac{g S^{+}_{0}}{\tilde{\tilde{\omega}}_{b} - i \frac{\kappa_{b}}{2}} 
    \\
    \label{g-prime-equals-zero-s-z-rotating}
    0 =& \left ( \alpha_{0}^{*} + \beta_{0} \right ) S^{-}_{0} - c.c.
    \end{align}}
\end{tabularx}
For $g^{\prime} \neq 0$, the time-dependent factors do not cancel, so a simple single-frequency rotating solution does not exist. 

Inserting \cref{g-prime-equals-zero-alpha-rotating,g-prime-equals-zero-beta-rotating} into \cref{g-prime-equals-zero-s-z-rotating} again sets a condition on the parameters:
\begin{equation}
    \label{rotating-condition}
    \frac{\kappa_{a}}{4 \, \tilde{\tilde{\omega}}_{a}^{2} + \kappa_{a}^{2}} = \frac{\kappa_{b}}{4 \, \tilde{\tilde{\omega}}_{b}^{2} + \kappa_{b}^{2}}
\end{equation}
However, with the extra degree of freedom provided by the rotation frequency of the frame, $\mu$, we can now find the solution in a larger region of parameter space by setting $\mu$ to satisfy \cref{rotating-condition}:
\begin{multline}
    \label{mu-general}
    \mu = \frac{2}{\kappa_{b} - \kappa_{a}} \Bigg [ \kappa_{a} \tilde{\omega}_{b} + \kappa_{b} \tilde{\omega}_{a} \\ \pm \sqrt{
    {\left ( \kappa_{a} \tilde{\omega}_{b}^{2} + \kappa_{b} \tilde{\omega}_{a}^{2} \right )}^{2} 
    + \left ( \kappa_{b} - \kappa_{a} \right ) \left ( \kappa_{a} \tilde{\omega}_{b}^{2} -\kappa_{b} \tilde{\omega}_{a}^{2} + \frac{1}{4} \kappa_{a} \kappa_{b} \left ( \kappa_{b} - \kappa_{a} \right ) \right )
    } \, \Bigg ]
\end{multline}
Derived under the assumption of $\kappa_{a}=\kappa_{b}$ the condition is $\tilde{\tilde{\omega}}_{a}^{2} = \tilde{\tilde{\omega}}_{b}^{2}$, leading to $\mu = \frac{1}{2} \left ( \tilde{\omega}_{a} - \tilde{\omega}_{b} \right )$.

Inserting \cref{g-prime-equals-zero-alpha-rotating,g-prime-equals-zero-beta-rotating} into \cref{g-prime-equals-zero-s-plus-rotating} yields:
\begin{multline}
    0 =
    \frac{\omega_{0} - \mu}{g^{2}} \left ( \tilde{\tilde{\omega}}_{a}^{2} + {\kappa^{\prime}_{a}}^{2} \right ) \left ( \tilde{\tilde{\omega}}_{b}^{2} + {\kappa^{\prime}_{b}}^{2} \right ) \\
    + 2 \left [ \left ( \tilde{\tilde{\omega}}_{a} - i {\kappa^{\prime}_{a}} \right ) \left ( \tilde{\tilde{\omega}}_{b}^{2} + {\kappa^{\prime}_{b}}^{2} \right )
    + \left ( \tilde{\tilde{\omega}}_{b} + i {\kappa^{\prime}_{b}} \right ) \left ( \tilde{\tilde{\omega}}_{a}^{2} + {\kappa^{\prime}_{a}}^{2} \right ) \right ] S^{z}_{0} \\
    + \left ( S^{2} - {S^{z}_{0}}^{2} \right ) \left [ U_{a} \left ( \tilde{\tilde{\omega}}_{b}^{2} + {\kappa^{\prime}_{b}}^{2} \right)
    + U_{b} \left ( \tilde{\tilde{\omega}}_{a}^{2} + {\kappa^{\prime}_{a}}^{2} \right) \right ]
\end{multline}
using $\abs{S^{+}_{0}}^{2} = S^{2} - {S^{z}_{0}}^{2}$ by spin magnitude conservation. This equation is solved numerically. 

In this case, as we have no further restraints on the equations, we can write:
\begin{equation}
    S^{+}_{0} = \sqrt{S^{2} - {S^{z}_{0}}^{2}} e^{i \phi}
\end{equation}
Therefore $S^{\pm}_{0}$ --- and by \cref{g-prime-equals-zero-alpha-rotating,g-prime-equals-zero-beta-rotating}, $\alpha_{0}$ and $\beta_{0}$ --- have a phase factor with free phase $\phi$ and so we have a $U(1)$ symmetry breaking. 

\subsection{Phase diagrams for \texorpdfstring{$U=0$}{U = 0}}

Plotting the phase diagrams for $U_{a}=U_{b}=0$ and $\kappa_{a}=\kappa_{b}=\kappa$ shows a structure somewhat similar to before, although transitions are suppressed to higher $g$ (\cref{zero-g-prime-omega_v_g-0}). In the $\omega_{a}$ versus $\omega_{b}$ plane at $g=1$ kHz, we see that $\abs{\omega_{a}}>\abs{\omega_{b}}$ gives the normal state and $\abs{\omega_{a}}<\abs{\omega_{b}}$ gives the inverted state (\cref{zero-g-prime-omega_a_v_omega_b-0}). This is the same behaviour as in the $g^{\prime}=g$ regime below the critical $g$ for superradiance. 

\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{zero-g-prime-omega_v_g-0}
    \caption[Phase diagram: $g^{\prime}=0$, $\omega$ vs $g$, $U_{a}=U_{b}=0$]{Phase diagram in the $\omega$ versus $g$ plane. Parameters are set such that $\omega_{a}=\omega_{b}=\omega$, $\kappa_{a}=\kappa_{b}=\kappa$ and $U_{a}=U_{b}=0$. Corresponds to the $g^{\prime}=g$ diagram \cref{equal-gs-u-zero-omega_v_g}.}
    \label{zero-g-prime-omega_v_g-0}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{zero-g-prime-omega_a_v_omega_b-0}
    \caption[Phase diagram: $g^{\prime}=0$, $\omega_{a}$ vs $\omega_{b}$, $U_{a}=U_{b}=0$]{As \cref{zero-g-prime-omega_v_g-0}, but in the $\omega_{a}$ versus $\omega_{b}$ plane at $g=1$ kHz.}
    \label{zero-g-prime-omega_a_v_omega_b-0}
\end{figure}

\subsection{Phase diagrams for \texorpdfstring{$U \neq 0$}{U != 0}}

For $\omega_{a}=\omega_{b}=\omega$, $\kappa_{a}=\kappa_{b}=\kappa$ and $U_{a}=-U_{b}=U$, the rotation frequency takes a simple form: $\mu=U S^{z}$. Thus, in the superradiant region the shifted two-level splitting becomes \mbox{$\tilde{\tilde{\omega}}_{0} = \omega_{0} - U S^{z}$}. Near the phase boundary, $S^{z} \approx S$. First taking $U$ such that \mbox{$\tilde{\tilde{\omega}}_{0} \approx \frac{\omega_{0}}{2}$} in \cref{zero-g-prime-u-more-under-omega_v_g-0}, we see the lobes of the superradiant region are moved in opposite directions in $g$. Swapping the sign of $U$ gives the same behaviour in reversed directions. Setting $U$ such that $\tilde{\tilde{\omega}}_{0} \approx \frac{\omega_{0}}{100}$ in \cref{zero-g-prime-u-under-omega_v_g-0}, we see that the boundaries are shifted to a greater degree. 

In \cref{zero-g-prime-u-over-omega_v_g-0}, we see that increasing $U$ such that the value of $\tilde{\tilde{\omega}}_{0}$ would become $-\frac{\omega_{0}}{100}$ if the superradiant phase boundary still existed creates some interesting behaviour. Zooming in by a factor of 10 on the $g \approx 0$ region in \cref{zero-g-prime-u-more-zoom-omega_v_g-0} and by a factor of 100 in \cref{zero-g-prime-u-more-zoom-zoom-omega_v_g-0} shows that the superradiant phase boundary has disappeared for negative $\omega$ and superradiant phase is stable for all $g \approx 0$. This appears to be impossible as with no light-matter coupling there is no way to populate the light modes. However, the form of the superradiant solution for the one-mode nonequilibrium Dicke model in previous work (Eq.~(11) in \cite{PhysRevA.85.013817}) suggests that superradiance is possible for $g=0$ if $\omega_{0}=0$. This corresponds to the energy levels of the two-level system becoming degenerate. It is possible that the system is self-tuning to this arrangement.
\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{zero-g-prime-u-more-under-omega_v_g-0}
    \caption[Phase diagram: $g^{\prime}=0$, $\omega$ vs $g$, $U_{a}=-U_{b}=500$ mHz]{As \cref{zero-g-prime-omega_v_g-0}, but with $U_{a}=-U_{b}=500$ mHz.}
    \label{zero-g-prime-u-more-under-omega_v_g-0}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{zero-g-prime-u-under-omega_v_g-0}
    \caption[Phase diagram: $g^{\prime}=0$, $\omega$ vs $g$, $U_{a}=-U_{b}=930$ mHz]{As \cref{zero-g-prime-omega_v_g-0}, but with $U_{a}=-U_{b}=930$ mHz.}
    \label{zero-g-prime-u-under-omega_v_g-0}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{zero-g-prime-u-over-omega_v_g-0}
    \caption[Phase diagram: $g^{\prime}=0$, $\omega$ vs $g$, $U_{a}=-U_{b}=950$ mHz]{As \cref{zero-g-prime-omega_v_g-0}, but with $U_{a}=-U_{b}=950$ mHz.}
    \label{zero-g-prime-u-over-omega_v_g-0}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{zero-g-prime-u-more-zoom-omega_v_g-0}
    \caption[Phase diagram: $g^{\prime}=0$, $\omega$ vs $g$, $U_{a}=-U_{b}=950$ mHz, zoomed $\times10$]{Region of \cref{zero-g-prime-u-over-omega_v_g-0} near the origin enlarged by a factor of $10$.}
    \label{zero-g-prime-u-more-zoom-omega_v_g-0}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{zero-g-prime-u-more-zoom-zoom-omega_v_g-0}
    \caption[Phase diagram: $g^{\prime}=0$, $\omega$ vs $g$, $U_{a}=-U_{b}=950$ mHz, zoomed $\times100$]{Region of \cref{zero-g-prime-u-more-zoom-omega_v_g-0} near the origin further enlarged.}
    \label{zero-g-prime-u-more-zoom-zoom-omega_v_g-0}
\end{figure}
