\chapter{General Methods}
\label{general-methods}

We aim to map out the phase diagram of the two-mode-cavity Dicke model. 
Phase diagrams record which states both exist and are stable for each point in parameter space.
Thus, we must find stationary points of the dynamics and then test these for stability. 
The dynamics follow from the equations of motion, which we will derive from the Hamiltonian (\cref{two-mode-cavity-dicke-hamiltonian-microscopic-general}). 
These equations in the steady state describe the stationary points and on addition of a perturbation, the dynamics of system fluctuations, allowing stability testing. 

Having fixed points and fluctuation dynamics for the general model, we later solve these in limiting cases to produce the phase diagrams. 

\section{Equations of motion}
\label{equations-of-motion}

As all atoms are identical, we will adopt a more convenient notation using the collective spin terms defined for $j=\pm, x, y, z$ as:
\begin{equation}
    \label{collective-spin}
    \hat{S}^{j} = \sum_{i=1}^{N} \hat{s}^{j}_{i} = N \hat{s}^{j}_{i} \quad \forall i
\end{equation}
Note that the spin ladder operators are related to the spin Cartesian operators by $S^{\pm} = S^{x} \pm i S^{y}$ and thus $S^{-} = {S^{+}}^{*}$.\footnote{Superscript ${}^{*}$ denotes complex conjugation.}
Where $S=\frac{N}{2}$, total spin is given by spin magnitude conservation: 
\begin{align}
    S^{2} =& {S^{x}}^{2} + {S^{y}}^{2} + {S^{z}}^{2} = \abs{S^{+}}^{2} + {S^{z}}^{2}
\end{align}

The Hamiltonian is then rewritten with $\tilde{\omega}_{a,b}=\omega_{a,b} + U_{a,b} \hat{S}^{z}$ as:
\begin{multline}
    \label{two-mode-cavity-hamiltonian-collective-general}
    \hat{H} = 
    \tilde{\omega}_{a} \hat{a}^{\dagger} \hat{a} 
    + \tilde{\omega}_{b} \hat{b}^{\dagger} \hat{b} 
    + \omega_{0} \hat{S}^{z} \\
    + \bigg [ \Big ( 
        g_{a} \hat{a}^{\dagger} \hat{S}^{-} 
        + g_{b} \hat{b}^{\dagger} \hat{S}^{+} 
        + g_{a}^{\prime} \hat{a}^{\dagger} \hat{S}^{+} 
    + g_{b}^{\prime} \hat{b}^{\dagger} \hat{S}^{-} \Big ) + H.c. \bigg ]
\end{multline}

As the number of atoms and cavity mode populations are on a large scale ($N=10^{5}$), we can work in mean-field theory. 
Thus, we write the classical equations of motion with respect to the expectation values of the variable operators.
This follows from the Lindblad equation, which describes the time evolution of the density matrix, $\rho$, of an open quantum system. 
Allowing cavity loss rates $\kappa_{a}$ and $\kappa_{b}$ on mode $a$ and $b$ respectively, it is given by:
\begin{equation}
    \label{lindblad-equation}
    \dot{\hat{\rho}} = - i [\hat{H}, \hat{\rho}] + \frac{\kappa_{a}}{2} \mathcal{L}[\hat{a}] + \frac{\kappa_{b}}{2} \mathcal{L}[\hat{b}]
\end{equation}
where the Lindblad superoperator $\mathcal{L}$ is defined by:
\begin{equation}
    \label{lindblad-superoperator}
    \mathcal{L}[\hat{X}] = 2 \hat{X} \hat{\rho} \hat{X}^{\dagger} - \hat{X}^{\dagger} \hat{X} \hat{\rho} - \hat{\rho} \hat{X}^{\dagger} \hat{X} 
\end{equation}

As the expectation value of operator $\hat{X}$ is defined by $\braket{\hat{X}} = Tr(\hat{\rho} \hat{X})$, the equations of motion follow from insertion of the Lindblad equation (\cref{lindblad-equation}) into $\frac{d}{d t} \braket{\hat{X}} = Tr(\dot{\hat{\rho}} \hat{X})$. 
Defining the classical variables:
\begin{align}
    \alpha =& \langle \hat{a} \rangle & \beta =& \langle \hat{b} \rangle & S^{+} =& \langle \hat{S}^{+} \rangle & S^{z} =& \langle \hat{S}^{z} \rangle
\end{align}
we find the set of equations:
      
\noindent\begin{tabularx}{\linewidth}{X X}
{\begin{align}
    \label{proto-eom-alpha}
    \dot{\alpha} &= - \left ( i \braket{[\hat{a}, \hat{H}]} 
    + \frac{\kappa_{a}}{2} \alpha \right ) \\
    \label{proto-eom-spin-plus}
    \dot{S^{+}} &= - i \braket{[\hat{S}^{+}, \hat{H}]} 
    \end{align}} &
    {\begin{align}
    \label{proto-eom-beta}
    \dot{\beta} &= - \left ( i \braket{[\hat{b}, \hat{H}]} 
    + \frac{\kappa_{b}}{2} \beta \right ) \\
    \label{proto-eom-spin-z}
    \dot{S^{z}} &= - i \braket{[\hat{S}^{z}, \hat{H}]} 
    \end{align}}
\end{tabularx} 
Note that we have three complex variables ($\alpha$, $\beta$ and $S^{+}$) and one real variable ($S^{z}$). This sums to seven independent (real) components.

In order to progress, we must now use a semiclassical approximation when evaluating the expectation values: $\braket{\hat{X} \hat{Y}} = \braket{\hat{X}} \braket{\hat{Y}}$. 
This holds under mean-field theory. Defining: 
\begin{align}
    \label{omega-tilde}
    \tilde{\omega}_{a, b} &= \omega_{a, b} + U_{a, b} S^{z} \\
    \label{omega-0-tilde}
    \tilde{\omega}_{0}&=\omega_{0} + U_{a} \abs{\alpha}^{2} + U_{b} \abs{\beta}^{2} \\
    \label{gamma}
    \gamma &= g_{a} \alpha^{*} + {g_{a}^{\prime}}^{*} \alpha + {g_{b}}^{*} \beta + g_{b}^{\prime} \beta^{*}
\end{align}
the equations of motion are\footnote{$c.c.$ denotes the complex conjugate of the preceding term.}:
\begin{align}
    \label{eom-general-alpha}
    \dot{\alpha} &= - i \bigg [ \left ( \tilde{\omega}_{a} - i \frac{\kappa_{a}}{2} \right ) \alpha + g_{a} S^{-} + g_{a}^{\prime} S^{+} \bigg ] \\
    \label{eom-general-beta}
    \dot{\beta} &= - i \bigg [ \left ( \tilde{\omega}_{b} - i \frac{\kappa_{b}}{2} \right ) \beta + g_{b} S^{+} + g_{b}^{\prime} S^{-} \bigg ] 
\end{align}
\noindent\begin{tabularx}{\linewidth}{X X}
\EquationSpacing{\begin{align}
    \label{eom-general-spin-plus}
    \dot{S^{+}} = i \Big ( \tilde{\omega}_{0} S^{+}   - 2 \gamma S^{z} \Big ) 
\end{align}} &
\EquationSpacing{\begin{align}
    \label{eom-general-spin-z}
    \dot{S^{z}} = i \Big ( \gamma S^{-} - c.c. \Big ) 
\end{align}}
\end{tabularx}

We could equivalently express the equations of motion in terms of Cartesian spins, as shown in the appendices (\cref{eom-general-xyz-alpha,eom-general-xyz-beta,eom-general-xyz-spin-x,eom-general-xyz-spin-y,eom-general-xyz-spin-z}). In this basis, we have two complex variables and three real variables, again summing to seven components. 

\section{Steady states}
\label{steady-states}
Inserting the fixed point solution: 
\begin{align}
    \alpha=&\alpha_{0} & \beta=&\beta_{0} & S^{+}=&S^{+}_{0} & S^{z}=&S^{z}_{0}
\end{align}
such that $\dot{\alpha_{0}}=\dot{\beta_{0}}=\dot{S^{+}_{0}}=\dot{S^{z}_{0}}=0$ into the equations of motion yields the set of equations:

\noindent\begin{tabularx}{\linewidth}{X X}
{
\begin{align}
    \label{steady-general-alpha}
    \alpha_{0} &= - \frac{g_{a}^{\noprime} S_{0}^{-} + g_{a}^{\prime} S_{0}^{+}}{\tilde{\omega}_{a} - \frac{1}{2} i \kappa_{a}} 
\\
    \label{steady-general-spin-plus}
    \tilde{\omega}_{0} S^{+}_{0} &= 2 \gamma S^{z}_{0} 
\end{align}
} 
&
{
\begin{align}
    \label{steady-general-beta}
    \beta_{0} &= - \frac{g_{b}^{\noprime} S_{0}^{+} + g_{b}^{\prime} S_{0}^{-}}{\tilde{\omega}_{b} - \frac{1}{2} i \kappa_{b}}
\\
    \label{steady-general-spin-z}
    0 &= \gamma S^{-}_{0} - c.c.                   
\end{align}
}
\end{tabularx}

Again, this can also be expressed in the Cartesian spin basis, as shown by \cref{steady-general-xyz-alpha,steady-general-xyz-beta,steady-general-xyz-spin-x,steady-general-xyz-spin-y,steady-general-xyz-spin-z}.

\section{Solutions}
\label{solutions}
There are two families of solutions to these equations: the normal phases and the superradiant phase. Let us define states by the ket $\ket{\{s^{z}_{i}\},\alpha,\beta}$. 

The normal state, $\Downarrow \; = \ket{\downarrow\downarrow\downarrow...,0,0}$, has all atoms in the spin-down state and zero occupation of the cavity modes. This corresponds to $S^{z}_{0}=-S$ and $S^{\pm}=0$.

The inverted normal state, or inverted state, $\Uparrow \; = \ket{\uparrow\uparrow\uparrow...,0,0}$, has all atoms in the spin-up state and zero occupation of the cavity modes. This corresponds to $S^{z}_{0}=S$ and $S^{\pm}=0$.

The superradiant state, $\text{SR} = \ket{\uparrow\uparrow\downarrow...,\alpha \neq 0,\beta \neq 0}$, has atoms in some configuration which is not all-up or all-down and macroscopic occupation of the cavity modes. This corresponds to $\abs{S^{z}_{0}}<S$ and $S^{\pm}\neq0$.

Normal states are possible at all points in parameter space (although not necessarily stable), whereas the superradiant can only exist where spin conditions are satisfied as defined by the steady state spin equations.

\section{Linear stability}
\label{linear-stability}

In order to check for stability of a solution, we insert a perturbed stationary solution:
\begin{align}
\alpha=&\alpha_{0} + \delta \alpha & \beta=&\beta_{0} + \delta \beta & S^{+}=&S^{+}_{0} + \delta S^{+} & S^{z}=&S^{z}_{0} + \delta S^{z}
\end{align}
into the equations of motion. 
Discarding fluctuations of greater than linear order under the assumption of small perturbations, we find a set of equations which describe the dynamics of the fluctuations:
\begin{align}
    \dot{\delta \alpha} \, =&
    - i \bigg [ \left ( \tilde{\omega}_{a} - i \frac{\kappa_{a}}{2} \right ) \delta \alpha 
    + U_{a} \alpha_{0} \delta S^{z} + g_{a}^{\noprime} \delta S^{-} + g_{a}^{\prime} \delta S^{+} \bigg ] \\
    \dot{\delta \beta} \, =& 
    - i \bigg [ \left ( \tilde{\omega}_{b} - i \frac{\kappa_{b}}{2} \right ) \delta \beta 
    + U_{b} \beta_{0} \delta S^{z} + g_{b}^{\noprime} \delta S^{+} + g_{b}^{\prime} \delta S^{-} \bigg ] \\
    \begin{split}
        \dot{\delta S^{+}} =&                                                                               
        i \, \bigg \{ \tilde{\omega}_{0} \delta S^{+}                                                      
        + S^{+}_{0} \Big [ U_{a} \left ( \alpha_{0}^{*} \delta \alpha + c.c. \right )                      
        + U_{b} \left ( \beta_{0}^{*} \delta \beta + c.c. \right ) \Big ]                                  \\
        &- 2 \left [ \gamma \delta S^{z}                                                                    
        + S^{z}_{0} \Big ( g_{a}^{\noprime} {\delta \alpha}^{*} + {g_{a}^{\prime}}^{*} \delta \alpha       
        + {g_{b}^{\noprime}}^{*} \delta \beta + g_{b}^{\prime} {\delta \beta}^{*} \Big ) \right ] \bigg \} 
    \end{split}\\
    \dot{\delta S^{z}} =&
    i \Big \{ \left [ \gamma \delta S^{-} + S^{-}_{0} \left ( g_{a}^{\noprime} {\delta \alpha}^{*} + {g_{a}^{\prime}}^{*} \delta \alpha + {g_{b}^{\noprime}}^{*} \delta \beta + g_{b}^{\prime} {\delta \beta}^{*} \right ) \right ] - c.c. \Big \}
\end{align}

We now use an ansatz for a general form of the time dependence of the fluctuations:

\noindent\begin{tabularx}{\linewidth}{X X}
{\begin{align}
\label{delta-alpha} \delta \alpha &= a_{1} e^{-i \lambda t} + a_{2}^{*} e^{i \lambda^{*} t} \\
    \delta \beta &= b_{1} e^{-i \lambda t} + b_{2}^{*} e^{i \lambda^{*} t}
    \end{align}} &
    {\begin{align}
    \delta S^{+} &= c_{1} e^{-i \lambda t} + c_{2}^{*} e^{i \lambda^{*} t}                      \\
    \delta S^{z} &= d e^{-i \lambda t} + d^{*} e^{i \lambda^{*} t}
\end{align}} 
\end{tabularx}
Note that complex variables require two components while real variables only require one.

This choice is made as it expresses the fluctuations as an exponentially decaying and exponentially growing part. $\lambda$ is complex, so we can write:
\begin{align}
    \lambda = \Re(\lambda) + i \, \Im(\lambda)
\end{align}
Inserting this into \cref{delta-alpha} yields: 
\begin{align}
    \delta \alpha = a_{1} \exp \Big \{ \big [ -i \Re(\lambda) + \Im(\lambda) \big ] t \Big \} + a_{2}^{*} \exp \Big \{ \big [ i \Re(\lambda) + \Im(\lambda) \big ] t \Big \}
\end{align}
and the other fluctuations have the same form.

Thus, non-zero $\Re(\lambda)$ corresponds to oscillatory fluctuations. $\Im(\lambda) < 0$ corresponds to exponentially decaying fluctuations. For both of these cases, the solution is stable (for small fluctuations, as we are working to linear order). However, if \mbox{$\Im(\lambda) > 0$}, then fluctuations exponentially grow so the solution is unstable.

Inserting the ansatz and rearranging yields the set of equations:
\begin{align}
    \lambda a_{1} =& 
    \left ( \tilde{\omega}_{a} - i \frac{\kappa_{a}}{2} \right ) a_{1} 
    + g_{a}^{\prime} c_{1}
    + U_{a} \alpha_{0} d + g_{a} c_{2} \\
    \lambda a_{2} =& 
    - \left ( \tilde{\omega}_{a} + i \frac{\kappa_{a}}{2} \right ) a_{2} 
    - {g_{a}^{\noprime}}^{*} c_{1} 
    - {g_{a}^{\prime}}^{*} c_{2}
    - U_{a} \alpha_{0}^{*} d \\
    \lambda b_{1} =& 
    \left ( \tilde{\omega}_{b} - i \frac{\kappa_{b}}{2} \right ) b_{1}
    + g_{b}^{\noprime} c_{1} 
    + g_{b}^{\prime} c_{2}
    + U_{b} \beta_{0} d \\
    \lambda b_{2} =& 
    - \left ( \tilde{\omega}_{b} + i \frac{\kappa_{b}}{2} \right ) b_{2}
    - g_{b}^{\prime} c_{1}
    - g_{b}^{\noprime} c_{2} 
    - U_{b} \beta_{0}^{*} d
\end{align}
\begin{multline}
    \lambda c_{1} =
    \left ( 2 {g_{a}^{\prime}}^{*} S^{z}_{0} - U_{a} S_{0}^{+} \alpha_{0}^{*} \right ) a_{1} 
    + \left ( 2 {g_{a}^{\noprime}} S^{z}_{0} - U_{a} S_{0}^{+} \alpha_{0} \right ) a_{2} \\
    + \left ( 2 {g_{b}^{\noprime}}^{*} S^{z}_{0} - U_{b} S_{0}^{+} \beta_{0}^{*} \right ) b_{1}
    + \left ( 2 {g_{b}^{\prime}} S^{z}_{0} - U_{b} S_{0}^{+} \beta_{0} \right ) b_{2} 
    - \tilde{\omega}_{0} c_{1}
    + 2 \gamma d
\end{multline}
\begin{multline}
    \lambda c_{2} =
    + \left ( - 2 {g_{a}^{\noprime}}^{*} S^{z}_{0} + U_{a} S_{0}^{-} \alpha_{0}^{*} \right ) a_{1} 
    + \left ( - 2 {g_{a}^{\prime}} S^{z}_{0} + U_{a} S_{0}^{-} \alpha_{0} \right ) a_{2} \\
    + \left ( - 2 {g_{b}^{\prime}}^{*} S^{z}_{0} + U_{b} S_{0}^{-} \beta_{0}^{*} \right ) b_{1} 
    + \left ( - 2 {g_{b}^{\noprime}} S^{z}_{0} + U_{b} S_{0}^{-} \beta_{0} \right ) b_{2}
    + \tilde{\omega}_{0} c_{2}
    - 2 \gamma^{*} d
\end{multline}
\begin{multline}
    \lambda d =
    \left ( {g_{a}^{\noprime}}^{*} {S^{+}_{0}} - {g_{a}^{\prime}}^{*} {S^{-}_{0}} \right ) a_{1}
    + \left ( {g_{a}^{\prime}} {S^{+}_{0}} - {g_{a}^{\noprime}} {S^{-}_{0}} \right ) a_{2} \\
    + \left ( {g_{b}^{\prime}}^{*} {S^{+}_{0}} - {g_{b}^{\noprime}}^{*} {S^{-}_{0}} \right ) b_{1} 
    + \left ( {g_{b}^{\noprime}} {S^{+}_{0}} - {g_{b}^{\prime}} {S^{-}_{0}} \right ) b_{2}
    + \gamma^{*} c_{1} 
    - \gamma c_{2}
\end{multline}
This set of equations can be expressed using a matrix eigenvalue equation of the form $\lambda \mathbf{v} = \mathbf{M} \, \mathbf{v}$, where:
\begin{align}
    \mathbf{v} = {\left ( \begin{matrix}
        a_{1} & a_{2} & b_{1} & b_{2} & c_{1} & c_{2} & d
        \end{matrix} \right )}^{\mathbf{T}}
\end{align}
and
\begin{multline}\label{perturbation-matrix}
    \hspace{-5.5em}
    \mathbf{M} = \left (
    \begin{matrix}
        \tilde{\omega_{a}} - \frac{1}{2} i \kappa_{a}                             &   
        0                                                                         &   
        0                                                                         &   
        0 \\
        0                                                                         &   
        - \left ( \tilde{\omega_{a}} + \frac{1}{2} i \kappa_{a} \right )          &   
        0                                                                         &   
        0 \\
        0                                                                         &   
        0                                                                         &   
        \tilde{\omega_{b}} - \frac{1}{2} i \kappa_{b}                             &   
        0 \\
        0                                                                         &   
        0                                                                         &   
        0                                                                         &   
        - \left ( \tilde{\omega_{b}} + \frac{1}{2} i \kappa_{b} \right ) \\
        2 {g_{a}^{\prime}}^{*} S^{z}_{0} - U_{a} S^{+}_{0} {\alpha_{0}}^{*}       &   
        2 {g_{a}^{\noprime}} S^{z}_{0} - U_{a} S^{+}_{0} {\alpha_{0}}             &   
        2 {g_{b}^{\noprime}}^{*} S^{z}_{0} - U_{b} S^{+}_{0} {\beta_{0}}^{*}      &   
        2 {g_{b}^{\prime}} S^{z}_{0} - U_{b} S^{+}_{0} {\beta_{0}} \\
        - 2 {g_{a}^{\noprime}}^{*} S^{z}_{0} + U_{a} {S^{-}_{0}} {\alpha_{0}}^{*} &   
        - 2 {g_{a}^{\prime}} S^{z}_{0} + U_{a} {S^{-}_{0}} {\alpha_{0}}           &   
        - 2 {g_{b}^{\prime}}^{*} S^{z}_{0} + U_{b} S^{-}_{0} {\beta_{0}}^{*}      &   
        - 2 {g_{b}^{\noprime}} S^{z}_{0} + U_{b} S^{-}_{0} {\beta_{0}} \\
        {g_{a}^{\noprime}}^{*} {S^{+}_{0}} - {g_{a}^{\prime}}^{*} {S^{-}_{0}}     &   
        {g_{a}^{\prime}} {S^{+}_{0}} - {g_{a}^{\noprime}} {S^{-}_{0}}             &   
        {g_{b}^{\prime}}^{*} {S^{+}_{0}} - {g_{b}^{\noprime}}^{*} {S^{-}_{0}}     &   
        {g_{b}^{\noprime}} {S^{+}_{0}} - {g_{b}^{\prime}} {S^{-}_{0}} \\
    \end{matrix}
    \right. ...
    \\\\
    \left. ...
    \begin{matrix}
        g_{a}^{\prime}           &   
        g_{a}^{\noprime}         &   
        U_{a} \alpha_{0} \\
        - {g_{a}^{\noprime}}^{*} &   
        - {g_{a}^{\prime}}^{*}   &   
        - U_{a} {\alpha_{0}}^{*} \\
        g_{b}^{\noprime}         &   
        g_{b}^{\prime}           &   
        U_{b} \beta_{0} \\
        - {g_{b}^{\prime}}^{*}   &   
        - {g_{b}^{\noprime}}^{*} &   
        - U_{b} {\beta_{0}}^{*} \\
        - \tilde{\omega}_{0}     &   
        0                        &   
        2 \gamma \\
        0                        &   
        \tilde{\omega}_{0}       &   
        - 2 \gamma^{*} \\
        \gamma^{*}               &   
        - \gamma                 &   
        0 \\
    \end{matrix}
    \right )
\end{multline}        

Again, this calculation can also be expressed in the Cartesian spin basis. The resultant perturbation matrix is shown in \cref{cartesian-spin-perturbation-matrix}.

\section{Numerics}
\label{numerics}

We numerically diagonalise this perturbation matrix to find the eigenvalues $\lambda$. If any one of the eigenvalues satisfies $\Im(\lambda) > 0$, then the solution is unstable. Otherwise, we have found a stable solution.

\subsection{Normal states}
\label{numericals-normal}

At each point in parameter space, we take the value of $S^{z}_{0}$ to be $S$ for the normal state and $-S$ for the inverted state then insert this into the perturbation matrix to check for stability.

\subsection{Superradiant states}
\label{numerics-superradiant}

At each point in parameter space, we solve for the numerical solutions of the equation for $S^{z}_{0}$. 
Then we test whether each superradiant solution is possible, the condition for which follows from spin magnitude conservation. 
Where a state may exist, we check that it is stable by inserting the $S^{z}_{0}$ value into the perturbation matrix.

\subsection{Phase diagrams}

We generate phase diagrams to show the possible stable states at each point in a two-dimensional slice of parameter space, choosing the parameter ranges over regions of interesting behaviour. At each point, we check for stability of each possible state and record the combination of states that are both possible and stable. Each of these combinations is assigned a colour to build up a colourmap of states over the slice of parameter space. This colourmap is the phase diagram. 

These stationary points of the dynamics describe the phases of the light-matter system. In a closed system, the stationary point would correspond to the ground state of the system, \textit{i.e.} the state which minimises the energy of the system. The analogue of this in an open quantum system is called an attractor. Through the driven and dissipative effects of open systems, attractors do not have to minimise the energy; indeed, the inverted state maximises the energy. Thus, it is possible to have multiple stable attractors overlapping in parameter space as we see in the phase diagrams of \cref{limiting-cases}.

There are also regions where there are no fixed points, which are denoted by ``$?$'' in the legends of the phase diagrams. In order to categorise the phase of the system in these regions, we must study the behaviour shown by time evolving the equations of motion. This is possible through numerical integration. It is possible to find limit cycles, which are not steady but instead oscillatory solutions \cite{PhysRevA.91.051601}. Limit cycles are particularly novel, with their closed system analogue --- often called ``time crystals'' --- never having been observed experimentally \cite{PhysRevLett.109.160401}.
