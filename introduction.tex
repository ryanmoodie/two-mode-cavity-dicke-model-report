\chapter{Introduction}
\label{introduction}

We will theoretically investigate the two-mode-cavity Dicke model in order to understand the symmetries of the phase transitions of the system. 
First, we motivate the study of such artificial quantum systems. 
As its physics is well understood, we then introduce the theory of the single mode system: the Dicke model. 
We also describe how it has been effectively realised in recent experiments and discuss the symmetry exhibited in its superradiant phase transition. 
Finally, we detail the extension of this model to the two-mode-cavity case.  

\section{Quantum simulation}
\label{quantum-simulation}

Simple, well-controlled systems provide an immediate link between experiment and theoretical models, allowing us to probe quantum behaviour and test our understanding of the underlying physics.
Quantum models with more than a few degrees of freedom cannot be simulated by a classical computer due to the exponentially-scaling complexity of the calculations, so it is indeed necessary to construct artificial systems to explore their behaviour \cite{RevModPhys.86.153}.
Many such systems exist in cavity quantum electrodynamics with cold atoms, with modern experiments in this field allowing us to measure the applicability of generic models and motivate new theory. 

\section{Dicke model}
\label{dicke-model}

The Dicke model \cite{PhysRev.93.99} is the simplest description of such a coupled light-matter system. 
It describes the interaction of an ensemble of two-level atoms with a single light mode under the dipole approximation, \textit{i.e.} assuming that the cavity mode wavelength is much larger than the characteristic atomic size. 
Using units such that $\hbar = 1$, the Dicke Hamiltonian is:\footnote{$H.c.$ denotes the Hermitian conjugate of the preceding term.}

\begin{equation}
    \label{dicke-hamiltonian-microscopic}
    \hat{H} = 
    \omega \hat{a}^{\dagger} \hat{a} 
    + \sum_{i = 1}^{N} \bigg \{ \omega_{0} \hat{s}^{z}_{i} 
    + g \Big [ \left ( \hat{a}^{\dagger} \hat{s}^{-}_{i} 
        + \hat{a}^{\dagger} \hat{s}^{+}_{i} \right ) 
    + H.c. \Big ] \bigg \}
\end{equation}
describing the energy of the cavity mode photons, atoms and light-matter interaction, respectively. 
$\omega$ is the cavity mode frequency, $N$ is the number of atoms, $\omega_{0}$ is the frequency splitting of the atomic levels, $g$ is the ensemble light-matter (dipole) coupling strength, $i$ labels atoms and $\hat{a}$ and $\hat{a}^{\dagger}$ are the cavity mode annihilation and creation operators. 
Considering the two-level atoms like spin-$\frac{1}{2}$ particles, $\hat{s}^{z}_{i}$ is the spin-$z$ operator and $\hat{s}^{-}_{i}$ and $\hat{s}^{+}_{i}$ are the lowering and raising spin ladder operators. 

The first interaction term, $\hat{a}^{\dagger} \hat{s}^{-}_{i}$, describes photon emission with electronic de-excitation and its Hermitian conjugate, $\hat{a} \hat{s}^{+}_{i}$, describes photon absorption with electronic excitation. 
The other two terms are denoted the counter-rotating terms: $\hat{a}^{\dagger} \hat{s}^{+}_{i}$, emission with excitation; and $\hat{a} \hat{s}^{i}_{i}$, absorption with de-excitation. 
These terms are often neglected under the rotating wave approximation if their effect is dominated by that of the other interaction terms.

The model predicts a superradiant phase transition at critical dipole coupling strength: collective effects in the coherent state cause enhancement of radiation, leading to macroscopic occupation of the cavity mode \cite{PhysRevA.7.831,PhysRevA.8.1440,PhysRevE.67.066203,PhysRevLett.90.044101,HEPP1973360}. 
However, this is only possible under the omission of a diamagnetic term, $D {\left ( \hat{a} + \hat{a}^{\dagger} \right )}^{2}$, which the full expression includes \cite{PhysRevLett.35.432,Nataf2010,PhysRevA.19.301}. 
This term varies with atomic concentration, so under the assumption of dilute atoms, it is omitted to give the Dicke Hamiltonian. 
However, this assumption does not hold in the regime of critical dipole coupling as it is experimentally achieved through high atomic density (\textit{i.e.} by increasing $N$). 

\section{Experimental realisation}
\label{experiment-realisation}

\begin{figure}
    \centering
    \includegraphics[width=\SmallFigureWidth]{raman}
    \caption[Multilevel atomic system with Raman channels]{
    Atomic energy level scheme, adapted from Ref.~\cite{PhysRevA.75.013804}. 
    Solid red denotes cavity mode mediation, while dashed blue shows pump mediation. 
    $\ket{0}$ and $\ket{1}$ form the energy levels of the effective two-level system (for example, two stable ground state sublevels in the atomic hyperfine structure) with frequency splitting $\omega_{0}$. 
    $\ket{s}$ and $\ket{r}$ are additional excited electronic states used to form the Raman channels. 
    The detunings of the pumps from the transition frequencies, $\Delta_{s}$ and $\Delta_{r}$, ensures that these excited states are not populated. 
    $g_{s}$ and $g_{r}$ are dipole coupling strengths, associated with the shown transitions.}
    \label{raman-scheme}
\end{figure}

A recent theoretical proposal \cite{PhysRevA.75.013804} of a nonequilibrium Dicke model realised in an driven, dissipative system has prompted much related work \cite{Baumann2010}. 
The scheme uses interactions which are not sensitive to the omission of the diamagnetic term based on constructing effective two-level systems using multilevel atoms and pump-and-cavity-mediated Raman transitions (\cref{raman-scheme}) following studies of Raman scattering such as Ref.~\cite{PhysRevA.24.1980}. 

The cavity is laser-pumped and has loss rate $\kappa$, allowing the parameters of the system to be tuned by the frequencies and intensities of the lasers and the loss rate of the cavity. 
The states of an effective two-level system are coupled by a pair of Raman channels, allowing paths for transitions between $\ket{0}$ and $\ket{1}$. 
A pair of lasers drive the transitions $\ket{1} \leftrightarrow \ket{r}$ and $\ket{0} \leftrightarrow \ket{s}$. 
Excitation is induced by absorption and de-excitation by stimulated emission (spontaneous emission is neglected assuming strong lasers). 
The cavity mode mediates the $\ket{r} \leftrightarrow \ket{0}$ and $\ket{s} \leftrightarrow \ket{1}$ transitions. 

Assuming large detunings, $\Delta_{s}$ and $\Delta_{r}$, the excited states can be adiabatically eliminated. 
The system can then be described by the nonequilibrium\footnote{Note that parameters are now effective quantities rather than the bare quantities of the original Dicke model.}
Dicke Hamiltonian:
\begin{equation}
    \label{effective-dicke-hamiltonian}
    \hat{H} = \omega \hat{a}^{\dagger} \hat{a} + \sum_{i=1}^{N} \bigg \{ \hat{s}^{z}_{i} \left ( \omega_{0} + U \hat{a}^{\dagger} \hat{a} \right )
    + \Big [ \left ( g_{r} \hat{a}^{\dagger} \hat{s}^{-}_{i} + g_{s} \hat{a}^{\dagger} \hat{s}^{+}_{i} \right ) + H.c. \Big ]  \bigg \}
\end{equation}
where characteristic energy scales are associated with optical frequency shifts and Raman transition rates rather than those of the photons and dipole couplings. 
For example, $\omega$ is now related to the difference between the cavity mode and pump frequencies (\textit{n.b.} as $\omega$ is a frequency difference, we will explore the parameter space of negative $\omega$ as well as positive). 
Note that the effective dipole coupling terms are denoted by the Raman channel they mediate. 
Light in the cavity causes a Stark shift in the energy levels of the atoms and in turn the atoms affect the permittivity of the cavity medium, so we introduce a feedback term, $U$, to describe this effect.

It is clear that this Hamiltonian has a discrete $\mathbb{Z}_{2}$ symmetry\footnote{$\mathbb{Z}_{2}$ refers to the cyclic group of order two, corresponding to the group of parity.}, \textit{i.e.} it is conserved under the transformation $(\hat{a}, \hat{b}, \hat{S}^{\pm}) \rightarrow -(\hat{a},\hat{b},\hat{S}^{\pm})$.

\section{Symmetry breaking}
\label{symmetry-breaking}

The superradiant phase in the nonequilibrium Dicke model has been found to correspond to a state of atomic self-organisation \cite{Baumann2010,Nagy2008,PhysRevLett.104.130401}, complemented by previous studies \cite{PhysRevLett.91.203001}. This has prompted much related work \cite{Mottl1570,Kollar2017,Brennecke16072013}. 

\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{chequerboard}
    \caption[Effective Dicke model experimental system]{Experimental system (taken from Ref.~\cite{PhysRevA.85.013817}) showing cold atoms (red) and light fields (blue) in a cavity with pumping and loss (a) below threshold pump power and (b) above threshold for superradiance.}
    \label{chequerboard}
\end{figure}

The pump mode sets up a standing wave in the z-direction, while the cavity mode sets up a standing wave in the x-direction. Below threshold pumping strength, the cavity mode has zero occupation and atoms align in the nodes of the pump field (\cref{chequerboard} (a)). Above threshold, the superradiant phase is achieved and the cavity mode is macroscopically occupied by pump photons coherent scattered by the atoms. The pump and cavity fields interfere to form a square-lattice potential, causing the atomic condensate to assume a chequerboard pattern (\cref{chequerboard} (b)). This pattern forms Bragg planes which scatter pump light into the cavity, forming a self-sustaining cycle: cavity light induces the chequerboard, and the chequerboard induces cavity light.

There are two equally probable chequerboard wavefunctions occupying the odd and even sublattices respectively. The condensate assumes one of these through initial fluctuations then self-organisation. As the light fields associated with each sublattice are distinguished by sign, the $\mathbb{Z}_{2}$-symmetry is spontaneously broken \cite{PhysRevLett.107.140402} in the relative phase between the pump and cavity modes (equivalent to a spatial symmetry breaking in the atomic density).

Artificial systems exhibiting a continuous $U(1)$ symmetry\footnote{$U(1)$ refers to the group of unitary $1\times1$ square matrices, corresponding to the multiplicative group of all complex numbers of unit amplitude.} are the focus of much attention owing to their connection to physics such as Higgs and Goldstone modes \cite{Leonard2017}. By extending the nonequilibrium Dicke model to multiple modes, it is possible to achieve $U(1)$ symmetry \cite{PhysRevLett.112.173601}. For example, two cavities may be overlapped such that the chequerboard potentials compete \cite{Leonard2017,1704.05803}. The two $\mathbb{Z}_{2}$ symmetries of the cavity light fields are combined to form one $U(1)$ symmetry which exists along a sharp line in parameter space where the effective mode frequencies are equal.

\section{Two-mode-cavity extension}
\label{two-mode-cavity-extension}

We aim to instead explore a system exhibiting $U(1)$ symmetry over an extended range of parameter space rather than just a line: the extension of the nonequilibrium Dicke model to a single cavity with two modes. To explore the symmetry of the superradiant phase transition in this model, we will map out its phase diagram, analogous to previous studies of the single-mode model \cite{PhysRevLett.105.043001,PhysRevA.85.013817}.

\begin{figure}
    \centering
    \includegraphics[width=\SmallFigureWidth]{raman2}
    \caption[Raman scheme in two-mode-cavity model]{
    Two mode version of \cref{raman-scheme}, adapted from Ref.~\cite{PhysRevA.75.013804}. 
    Solid red shows mode $a$ mediated transitions, solid green for mode $b$ and dashed blue for the pump.}
    \label{raman-scheme-two-mode-cavity}
\end{figure}

Where symbols are used as before, with the two cavity modes denoted by the letters $a$ and $b$, the Hamiltonian of the two-mode-cavity Dicke model is:
\begin{multline}
    \label{two-mode-cavity-dicke-hamiltonian-microscopic-general}
    \hat{H} = \omega_{a} \hat{a}^{\dagger} \hat{a} + \omega_{b} \hat{b}^{\dagger} \hat{b} 
    + \sum_{i = 1}^{N} \Bigg \{ 
    \hat{s}^{z}_{i} \Big ( \omega_{0}  
    + U_{a} \hat{a}^{\dagger} \hat{a} 
    + U_{b} \hat{b}^{\dagger} \hat{b} \Big ) \\
    + \bigg [ \Big ( g_{a} \hat{a}^{\dagger} \hat{s}^{-}_{i} 
        + g_{b} \hat{b}^{\dagger} \hat{s}^{+}_{i} 
        + g_{a}^{\prime} \hat{a}^{\dagger} \hat{s}^{+}_{i} 
    + g_{b}^{\prime} \hat{b}^{\dagger} \hat{s}^{-}_{i} \Big ) + H.c. \bigg ] \Bigg \}
\end{multline}

We have independently labelled the dipole couplings associated with each transition such that defining values of primed terms with respect to the unprimed terms will allow us to explore limiting cases where we expect to find $U(1)$ symmetry. The transitions are shown labelled by the corresponding dipole coupling in \cref{raman-scheme-two-mode-cavity}.
