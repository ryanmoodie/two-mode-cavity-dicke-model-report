\section{Case: \texorpdfstring{$g^{\prime}=g$, $\, g \in \mathbb{R}$}{g prime = g, g real}}
\label{g-prime-equals-g}

The regime of $g^{\prime}=g$ with real $g$ is investigated first as it is a point of high symmetry, \textit{i.e.} the Hamiltonian and equations of motion take a simple form. Therefore, we expect the equations will be easy to work with. It also allows us to define parameters such that we can map back to the one-cavity-mode model to verify that the simulation which produces the phase diagrams works correctly.

The Hamiltonian simplifies to:
\begin{equation}
    \hat{H} = 
    \tilde{\omega}_{a} \hat{a}^{\dagger} \hat{a} 
    + \tilde{\omega}_{b} \hat{b}^{\dagger} \hat{b} 
    + \omega_{0} \hat{S}^{z} 
    + 2 g \bigg [ \Big ( 
        \hat{a}
        + \hat{b}
         \Big ) + H.c. \bigg ] \hat{S}^{x}
\end{equation}
In the steady state, the equations of motion (\cref{g-prime-equals-g-eom-alpha,g-prime-equals-g-eom-beta,g-prime-equals-g-eom-spin-plus,g-prime-equals-g-eom-spin-z}) become:

\noindent\begin{tabularx}{\linewidth}{X p{14.5em}}
{\begin{align}
    \label{g-prime-equals-g-alpha-steady}
    \alpha_{0} &= - \frac{2 g S^{x}_{0}}{\tilde{\omega}_{a} - i \frac{\kappa_{a}}{2}} \\
    \label{g-prime-equals-g-s-plus-steady}
    \tilde{\omega}_{0} S^{+}_{0}
    &= 2 g \Big [ \left ( \alpha_{0} + \beta_{0} \right ) + c.c. \Big ] S^{z}_{0} 
\end{align}} &
{\begin{align}
    \label{g-prime-equals-g-beta-steady}
    \beta_{0} &= - \frac{2 g S^{x}_{0}}{\tilde{\omega}_{b} - i \frac{\kappa_{b}}{2}} 
    \\
    \label{g-prime-equals-g-s-z-steady}
    0 &= \Big [ \left ( \alpha_{0} + \beta_{0} \right ) + c.c. \Big ] S^{y}_{0}
    \end{align}}
\end{tabularx}

\subsection{Solutions}

The usual normal states exist along with two superradiant states corresponding to the two possible solutions of \cref{g-prime-equals-g-s-z-steady}. 

We call the first superradiant solution, corresponding to $S^{y}_{0}=0$, SRA. In this case, inserting \cref{g-prime-equals-g-alpha-steady,g-prime-equals-g-beta-steady} into \cref{g-prime-equals-g-s-plus-steady} yields a quartic equation for $S^{z}$:
\begin{multline}
    \omega_{0} \left ( \tilde{\omega}_{a}^{2} + \frac{\kappa_{a}^{2}}{4} \right ) \left ( \tilde{\omega}_{b}^{2} + \frac{\kappa_{b}^{2}}{4} \right ) \\
    + 4 g^{2} \left ( S^{2} - {S^{z}_{0}}^{2} \right ) 
    \left ( U_{a} \tilde{\omega}_{b}^{2} + U_{b} \tilde{\omega}_{a}^{2} + U_{a} \frac{\kappa_{b}^{2}}{4} + U_{b} \frac{\kappa_{a}^{2}}{4} \right ) \\
    + 8 g^{2} S^{z}_{0} \left ( \tilde{\omega}_{a} \tilde{\omega}_{b} \left ( \tilde{\omega}_{a}
    + \tilde{\omega}_{b} \right ) + \frac{\kappa_{a}^{2}}{4} \tilde{\omega}_{b} + \frac{\kappa_{b}^{2}}{4} \tilde{\omega}_{a} \right ) = 0
\end{multline}
using $S^{+}_{0} = S^{x}_{0} = \pm \sqrt{S^{2} - {S^{z}_{0}}^{2}}$ by spin magnitude conservation. The quartic equation is solved numerically. 

We insert these forms for the spins into the perturbation matrix defined in \cref{perturbation-matrix} to perform stability tests. SRA solutions only exist for $\abs{S^{z}_{0}} < S$ such that $S^{x}_{0}$ is real and non-zero.  

The other superradiant state, SRB, is the solution for which in general the light fields are purely imaginary, \textit{i.e.} the condition:
\begin{equation}
\left ( \alpha_{0} + \beta_{0} \right ) + c.c. = 0
\end{equation}
Then by \cref{g-prime-equals-g-s-plus-steady}, $\tilde{\omega}_{0} = 0$ (\textit{n.b.} $S^{+}_{0}=0$ is a normal solution). Solving these two equations and using conservation of spin magnitude to define $S^{y}_{0}$ leads to the set of spin equations: 
\begin{gather}
    S^{x}_{0} = \pm \frac{1}{2 g} \sqrt{- \omega_{0} \frac
        {\left ( \tilde{\omega}_{a}^{2} + \frac{\kappa_{a}^{2}}{4} \right ) \left ( \tilde{\omega}_{b}^{2} + \frac{\kappa_{b}^{2}}{4} \right )}
        {U_{a} \left ( \tilde{\omega}_{b}^{2} + \frac{\kappa_{b}^{2}}{4} \right ) + U_{b} \left ( \tilde{\omega}_{a}^{2} + \frac{\kappa_{a}^{2}}{4} \right )}} \\
    \label{spin-y-srb}
    S^{y}_{0} = \pm \sqrt{S^{2} - {S^{x}_{0}}^{2} - {S^{z}_{0}}^{2}} \\
    \begin{aligned}
        {S^{z}_{0}}^{3} \left [ U_{a} U_{b} \left ( U_{a} + U_{b} \right ) \right ]                                                              
        + {S^{z}_{0}}^{2} \left [ \omega_{a} U_{b} \left ( U_{b} + 2 U_{a} \right ) + \omega_{b} U_{a} \left ( U_{a} + 2 U_{b} \right ) \right ] \\
        + {S^{z}_{0}} \left \{ U_{a} \left [ \omega_{b} \left ( 2 \omega_{a} + \omega_{b} \right ) + \frac{\kappa_{b}^{2}}{4} \right ]           
        + U_{b} \left [ \omega_{a} \left ( 2 \omega_{b} + \omega_{a} \right ) + \frac{\kappa_{a}^{2}}{4} \right ] \right \}                      \\
        + \omega_{a} \left ( \omega_{b}^{2} + \frac{\kappa_{b}^{2}}{4} \right )                                                                  
        + \omega_{b} \left ( \omega_{a}^{2} + \frac{\kappa_{a}^{2}}{4} \right ) = 0                                                              
    \end{aligned}
\end{gather}
where the cubic equation in $S^{z}_{0}$ is solved numerically. 

As it is easier to express the SRB solution in the Cartesian spin basis, we insert the forms of the spins into the perturbation matrix defined in \cref{cartesian-spin-perturbation-matrix} for stability calculations. For solutions to be possible, ${S^{x}_{0}}^{2}+{S^{z}_{0}}^{2} \leq S^{2}$ must be satisfied so that $S^{y}$ is real (by \cref{spin-y-srb}). 

Both of these superradiant solutions have the $x$ and $y$ spin components determined up to a sign, which corresponds to a discrete $\mathbb{Z}_{2}$ symmetry breaking. Thus, we see that no continuous $U(1)$ symmetry exists for this regime as the solutions are not parameterised by some free variable. 

\subsection{Relation to one-mode case}

Taking $\omega_{a} = \omega_{b} = \omega$, $\kappa_{a}=\kappa_{b}=\kappa$ and $U_{a}=U_{b}=U$, the Hamiltonian becomes, with $\tilde{\omega}=\omega + U S^{z}$:
\begin{equation}
    \hat{H} = 
    \tilde{\omega} \left ( \hat{a}^{\dagger} \hat{a} 
    +  \hat{b}^{\dagger} \hat{b} \right )
    + \omega_{0} \hat{S}^{z} 
    + 2 g \bigg [ \Big ( 
        \hat{a}
        + \hat{b}
         \Big ) + H.c. \bigg ] \hat{S}^{x}
\end{equation}
Now let us make the change of basis:
\begin{align}
\hat{a} =& \frac{1}{\sqrt{2}}\left ( \hat{c} + \hat{d} \right ) & \hat{b} =& \frac{1}{\sqrt{2}} \left ( \hat{c} - \hat{d} \right )
\end{align}
to find:
\begin{equation}
    \hat{H} = 
    \tilde{\omega} \hat{c}^{\dagger} \hat{c} 
    + \omega_{0} \hat{S}^{z} 
    + 2 \sqrt{2} g \Big ( 
        \hat{c}^{\dagger}
        + \hat{c}
         \Big ) \hat{S}^{x}
    + \tilde{\omega} \hat{d}^{\dagger} \hat{d}
\end{equation}

This Hamiltonian describes two independent modes. Mode $d$, with loss rate $\kappa$ and no driving, does nothing. Mode $c$ couples to the pump and, up to a factor of $\sqrt{2}$ wherever $g$ appears, maps to the one-mode nonequilibrium Dicke Hamiltonian (\textit{c.f.} \cref{effective-dicke-hamiltonian}, now in Cartesian collective spin basis). $\omega$ versus $g$ phase diagrams for some $U$ values (\cref{equal-gs-u-zero-omega_v_g,equal-gs-neg-equal-us-omega_v_g,equal-gs-pos-equal-us-omega_v_g}) agree (to factor $\sqrt{2}$) with the results of studies of the one-mode case (see Figs.~4 and 13 in Ref.~\cite{PhysRevA.85.013817}). This demonstrates the simulation working correctly. 

The main features of the phase diagrams of Ref.~\cite{PhysRevA.85.013817} are apparent. For $U=0$, \cref{equal-gs-u-zero-omega_v_g} shows the textbook nonequilibrium Dicke model phase diagram. For $U<0$, \cref{equal-gs-neg-equal-us-omega_v_g} shows the existence of the SRB phase and the horizontal boundaries of the normal phases moving over one another. \cref{equal-gs-pos-equal-us-omega_v_g} shows that for $U>0$, the normal phase boundaries separate to greater $\abs{\omega}$. In the gap created, there are no steady states; analysis of the time dynamics shows that a limit cycle exists here \cite{PhysRevA.85.013817}.
\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{equal-gs-u-zero-omega_v_g-0}
    \caption[Phase diagram: $g^{\prime}=g$, $\omega$ vs $g$, $U_{a}=U_{b}=0$]{Phase diagram in the $\omega$ versus $g$ plane. Parameters are set such that $\omega_{a}=\omega_{b}=\omega$, $\kappa_{a}=\kappa_{b}=\kappa$ and $U_{a}=U_{b}=U=0$. }
    \label{equal-gs-u-zero-omega_v_g}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{equal-gs-neg-equal-us-omega_v_g-0}
    \caption[Phase diagram: $g^{\prime}=g$, $\omega$ vs $g$, $U_{a}=U_{b}<0$]{As \cref{equal-gs-u-zero-omega_v_g}, but with $U=-400$ Hz.}
    \label{equal-gs-neg-equal-us-omega_v_g}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{equal-gs-pos-equal-us-omega_v_g-0}
    \caption[Phase diagram: $g^{\prime}=g$, $\omega$ vs $g$, $U_{a}=U_{b}>0$]{As \cref{equal-gs-u-zero-omega_v_g}, but with $U=400$ Hz.}
    \label{equal-gs-pos-equal-us-omega_v_g}
\end{figure}

\clearpage

\subsection{Additional axis}

Now returning to the two-mode-cavity model, we can investigate a new axis of parameter space: the second mode frequency, $\omega_{b}$. Plotting $\omega_{a}$ against $\omega_{b}$ at $g=1$ kHz (\cref{equal-gs-u-zero-omega_a_v_omega_b,equal-gs-neg-equal-us-omega_a_v_omega_b,equal-gs-pos-equal-us-omega_a_v_omega_b}) for the three cases in $U_{a}=U_{b}=U$ (with $\kappa_{a}=\kappa_{b}=\kappa$) of the previous section shows us behaviour for each in an orthogonal plane which intersects the $\omega$ versus $g$ phase diagrams on the vertical line $g=1$ kHz. Equivalently, the $\omega$ versus $g$ diagrams intersect the $\omega_{a}$ versus $\omega_{b}$ diagrams on the diagonal line $\omega_{a}=\omega_{b}$. For each pair, we see the phases matching on this line of intersection. 
\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{equal-gs-u-zero-omega_a_v_omega_b-0}
    \caption[Phase diagram: $g^{\prime}=g$, $\omega_{a}$ vs $\omega_{b}$, $U_{a}=U_{b}=0$]{Phase diagram in the $\omega_{a}$ versus $\omega_{b}$ plane. Parameters are set such that $\kappa_{a}=\kappa_{b}=\kappa$, $U_{a}=U_{b}=U=0$ and $g=1$ kHz. Corresponds to \cref{equal-gs-u-zero-omega_v_g}.}
    \label{equal-gs-u-zero-omega_a_v_omega_b}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{equal-gs-neg-equal-us-omega_a_v_omega_b-0}
    \caption[Phase diagram: $g^{\prime}=g$, $\omega_{a}$ vs $\omega_{b}$, $U_{a}=U_{b}<0$]{As \cref{equal-gs-u-zero-omega_a_v_omega_b}, but with $U=-400$ Hz. Corresponds to \cref{equal-gs-neg-equal-us-omega_v_g}.}
    \label{equal-gs-neg-equal-us-omega_a_v_omega_b}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{equal-gs-pos-equal-us-omega_a_v_omega_b-0}
    \caption[Phase diagram: $g^{\prime}=g$, $\omega_{a}$ vs $\omega_{b}$, $U_{a}=U_{b}>0$]{As \cref{equal-gs-u-zero-omega_a_v_omega_b}, but with $U=400$ Hz. Corresponds to \cref{equal-gs-pos-equal-us-omega_v_g}.}
    \label{equal-gs-pos-equal-us-omega_a_v_omega_b}
\end{figure}

\clearpage

\subsection{Opposite feedback strengths on modes}

Taking $\kappa_{a}=\kappa_{b}=\kappa$ and $U_{a}=-U_{b}=U$ gives another subregime of high symmetry. The SRA solution simplifies to the quartic expression in $S^{z}_{0}$:
\begin{multline}
    \omega_{0} \left ( \tilde{\omega}_{a}^{2} + \frac{\kappa^{2}}{4} \right ) \left ( \tilde{\omega}_{b}^{2} + \frac{\kappa^{2}}{4} \right )
    + 4 U g^{2} \left ( S^{2} - {S^{z}_{0}}^{2} \right ) 
    \left ( \tilde{\omega}_{b}^{2} - \tilde{\omega}_{a}^{2} \right ) \\
    + 8 g^{2} S^{z}_{0} \left ( \tilde{\omega}_{a} \tilde{\omega}_{b} + \frac{\kappa^{2}}{4} \right ) \left ( \tilde{\omega}_{a} 
    + \tilde{\omega}_{b} \right ) = 0
\end{multline}
and for SRB the spin components are expressed by:
\begin{gather}
    S^{x}_{0} = \pm \frac{1}{2 g} \sqrt{- \frac{\omega_{0}}{U} \frac
        {\left ( \tilde{\omega}_{a}^{2} + \frac{\kappa^{2}}{4} \right ) \left ( \tilde{\omega}_{b}^{2} + \frac{\kappa^{2}}{4} \right )}
        {\tilde{\omega}_{b}^{2} - \tilde{\omega}_{a}^{2}}} \\
    S^{y}_{0} = \pm \sqrt{S^{2} - {S^{x}_{0}}^{2} - {S^{z}_{0}}^{2}} \\
    S^{z}_{0} = \frac{1}{2 U} \left [ \omega_{b} - \omega_{a} \pm \sqrt{{\left ( \omega_{a} + \omega_{b} \right )}^{2} + \kappa^{2}} \, \right ]
\end{gather}

Plotting phase diagrams in this case (\cref{equal-gs-neg-opposite-us-omega_v_g-0,equal-gs-neg-opposite-us-omega_a_v_omega_b-0}) shows similar results as for $U_{a}=U_{b}$, but unsurprisingly with more complex structure.
\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{equal-gs-neg-opposite-us-omega_v_g-0}
    \caption[Phase diagram: $g^{\prime}=g$, $\omega$ vs $g$, $U_{a}=-U_{b}$]{Phase diagram in the $\omega$ versus $g$ plane, with \mbox{$\omega_{a}=\omega_{b}=\omega$}, \mbox{$\kappa_{a}=\kappa_{b}=\kappa$} and \mbox{$U_{a}=-U_{b}=-400$ Hz}.}
    \label{equal-gs-neg-opposite-us-omega_v_g-0}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[height=\FigureHeight]{equal-gs-neg-opposite-us-omega_a_v_omega_b-0}
    \caption[Phase diagram: $g^{\prime}=g$, $\omega_{a}$ vs $\omega_{b}$, $U_{a}=-U_{b}$]{As \cref{equal-gs-neg-opposite-us-omega_v_g-0}, but in the $\omega_{a}$ versus $\omega_{b}$ plane at $g=1$ kHz.}
    \label{equal-gs-neg-opposite-us-omega_a_v_omega_b-0}
\end{figure}
